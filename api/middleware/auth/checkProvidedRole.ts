import { User } from '../../db/Models';

export const hasAdminRole = (req, res, next) => {
    User.findByPk(req.userId).then((user) => {
        if (!user)
            return res.status(403).send({
                message: 'Пользователь не найден',
            });

        user.getRoles().then((roles) => {
            for (const role of roles) {
                if (role.name === 'admin') {
                    next();
                    return;
                }
            }

            return res.status(403).send({
                message: 'Требуется роль администратора!',
            });
        });
    });
};

import { verify } from 'jsonwebtoken';
// Configs
import { AuthConfig } from '../../configs/authConfig';

export const verifyTokenWithJwt = (req, res, next) => {
    const token = req.headers['x-access-token'];

    if (req.path.includes('/api/auth')) {
        next();
    } else if (!token) {
        res.status(403).send({
            message: 'Необходима авторизация',
        });
    } else {
        verify(token, AuthConfig.secret, (err, decoded) => {
            if (err) {
                return res.status(401).send({
                    message: 'Авторизация провалилась!',
                });
            }
            req.userId = decoded.id;
            next();
        });
    }
};

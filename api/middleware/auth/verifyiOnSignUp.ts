import { User } from '../../db/Models';

export const checkDuplicatesOnRegistration = (req, res, next) => {
    const getUserName = User.findOne({
        where: {
            username: req.body.username,
        },
    });

    Promise.all([getUserName]).then(([user]) => {
        if (user) {
            res.status(400).send({
                message: 'Пользователь с таким именем существует',
            });
        } else {
            next();
        }
    });
};

import express, { Application, Router } from 'express';

type Controller = {
    path: string;
    controller: Router;
};

class App {
    public app: Application;
    public port: number;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor(appInit: { port: number; middleWares: any; controllers: Controller[] }) {
        this.app = express();
        this.port = appInit.port;

        this.middlewares(appInit.middleWares);
        this.routes(appInit.controllers);
        this.assets();
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private middlewares(middleWares: { forEach: (arg0: (middleWare: any) => void) => void }) {
        middleWares.forEach((middleWare) => {
            this.app.use(middleWare);
        });
    }

    private routes(controllers: Controller[]) {
        controllers.forEach(({ path, controller }) => {
            this.app.use(`/api/${path}`, controller);
        });
    }

    private assets() {
        this.app.use(express.static('public'));
    }

    public listen() {
        this.app.listen(this.port, () => {
            // eslint-disable-next-line no-console
            console.log(`App listening on the http://localhost:${this.port}`);
        });
    }
}

export default App;

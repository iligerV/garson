import * as bodyParser from 'body-parser';
import { errorHandling, verifyTokenWithJwt } from './middleware';
// MainClass
import App from './app';
// Controllers
import { indexRouter, basketRouter, menuRouter, profileRouter, authRouter } from './controllers';
// Types
import { APP_ROUT } from '../client/src/Models';
import { sequelize } from './db';

const app = new App({
    port: 3001,
    controllers: [
        { path: '', controller: indexRouter },
        { path: APP_ROUT.MENU, controller: menuRouter },
        { path: APP_ROUT.BASKET, controller: basketRouter },
        { path: APP_ROUT.PROFILE, controller: profileRouter },
        { path: 'auth', controller: authRouter },
    ],
    middleWares: [
        bodyParser.json(),
        bodyParser.urlencoded({ extended: true }),
        verifyTokenWithJwt,
        errorHandling,
    ],
});

app.listen();
// eslint-disable-next-line no-console
sequelize.sync().catch((err) => console.log(err));

module.exports = {
    up: async (queryInterface) => {
        return queryInterface.bulkInsert('users', [
            {
                username: 'Administrator',
                password: 'Administrator',
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ]);
    },

    down: async (queryInterface) => {
        return queryInterface.bulkDelete('users', null, {});
    },
};

module.exports = {
    up: async (queryInterface) => {
        return queryInterface.bulkInsert('roles', [
            {
                id: 0,
                name: 'user',
            },
            {
                id: 1,
                name: 'admin',
            },
        ]);
    },

    down: async (queryInterface) => {
        return queryInterface.bulkDelete('roles', null, {});
    },
};

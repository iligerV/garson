import { Sequelize } from 'sequelize';
import dataBaseConfig from '../configs/dataBase.json';

const env = process.env.NODE_ENV || 'development';
const { username, password, database, host, dialect } = dataBaseConfig[env];

export const sequelize = new Sequelize(database, username, password, {
    dialect,
    host,
    define: {
        freezeTableName: true,
    },
});

export * from './Models';

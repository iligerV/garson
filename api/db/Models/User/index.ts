import {
    DataTypes,
    HasManyAddAssociationMixin,
    HasManyCountAssociationsMixin,
    HasManyCreateAssociationMixin,
    HasManyGetAssociationsMixin,
    HasManyHasAssociationMixin,
    HasManySetAssociationsMixin,
    Model,
    Optional,
} from 'sequelize';
import { sequelize } from '../../index';
import { RoleModel } from '../Role';

export interface UserAttributes {
    id: number;
    username: string;
    password: string;
}

export interface UserCreationAttributes extends Optional<UserAttributes, 'id'> {}

class User extends Model<UserAttributes, UserCreationAttributes> implements UserAttributes {
    public id!: number;
    public username!: string;
    public password!: string;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;

    public getRoles!: HasManyGetAssociationsMixin<typeof RoleModel>;
    public setRoles!: HasManySetAssociationsMixin<typeof RoleModel, number>;
    public addRole!: HasManyAddAssociationMixin<typeof RoleModel, number>;
    public hasRole!: HasManyHasAssociationMixin<typeof RoleModel, number>;
    public countRoles!: HasManyCountAssociationsMixin;
    public createRole!: HasManyCreateAssociationMixin<typeof RoleModel>;

    public static associate() {
        this.belongsToMany(RoleModel, {
            through: 'user_roles',
            foreignKey: 'userId',
            otherKey: 'roleId',
        });
    }
}

User.init(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        username: {
            type: DataTypes.STRING(128),
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING(128),
            allowNull: false,
        },
    },
    { sequelize, modelName: 'users' }
);

export const UserModel = User;

import { RoleModel } from './Role';
import { UserModel } from './User';

[UserModel, RoleModel].forEach((model) => model.associate?.());

export { RoleModel as Role, UserModel as User };

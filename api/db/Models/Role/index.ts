import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../index';
import { UserModel } from '../User';

export enum ROLE {
    USER,
    ADMIN,
}

export interface RoleAttributes {
    id: number;
    name: string;
}

export interface RoleCreationAttributes extends RoleAttributes {}

class Role extends Model<RoleAttributes, RoleCreationAttributes> implements RoleAttributes {
    public id!: number;
    public name!: string;

    public static associate() {
        this.belongsToMany(UserModel, {
            through: 'user_roles',
            foreignKey: 'roleId',
            otherKey: 'userId',
        });
    }
}

Role.init<Model<RoleAttributes>>(
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        name: {
            type: DataTypes.STRING(128),
            allowNull: false,
        },
    },
    {
        sequelize,
        createdAt: false,
        updatedAt: false,
        modelName: 'roles',
    }
);

export const RoleModel = Role;

import { Router } from 'express';
// DB
import { User } from '../db/Models';

const router = Router();

router.get('/', async (req, res) => {
    const { name } = req.query;
    res.send(name);
    // http://192.168.1.69:8082/api/profile?name=JANE
    // User.create({ name: 'Jane' })
    //     .then((data) => {
    //         console.log(data.toJSON());
    //
    //         res.send(data);
    //         return data;
    //     })
    //     .then((data) => data.destroy())
    //     .catch((err) => res.send(err));
});

export const profileRouter = router;

import { Router } from 'express';
import { sign } from 'jsonwebtoken';
import { hashSync, compareSync } from 'bcryptjs';
// Configs
import { AuthConfig } from '../configs/authConfig';
// DB
import { User } from '../db/Models';
// Middleware
import { checkDuplicatesOnRegistration } from '../middleware';
// Types
import { ROLE } from '../db/Models/Role';

const router = Router();

router.post('/signup', [checkDuplicatesOnRegistration], async (req, res) => {
    User.create({
        username: req.body.username,
        password: hashSync(req.body.password, 8),
    })
        .then((user) => {
            user.setRoles([ROLE.USER]).then(() => {
                res.send({ message: 'Регистрация прошла успешно' });
            });
        })
        .catch((err) => {
            res.status(500).send({ message: err.message });
        });
});
router.post('/signin', async (req, res) => {
    User.findOne({
        where: {
            username: req.body.username,
        },
    })
        .then((user) => {
            if (!user) return res.status(404).send({ message: 'Пользователь не найден' });

            if (!compareSync(req.body.password, user.password)) {
                return res.status(401).send({
                    accessToken: null,
                    message: 'Неверный пароль!',
                });
            }

            const token = sign({ id: user.id }, AuthConfig.secret, {
                expiresIn: 3600 * 24, // 24 hours
            });

            user.getRoles().then((roles) => {
                res.status(200).send({
                    id: user.id,
                    username: user.username,
                    roles: roles.map((currentRole) => currentRole.name.toUpperCase()),
                    accessToken: token,
                });
            });
        })
        .catch((err) => {
            res.status(500).send({ message: err.message });
        });
});

export const authRouter = router;

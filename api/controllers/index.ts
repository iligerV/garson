import { Router } from 'express';

export * from './basket';
export * from './menu';
export * from './profile';
export * from './auth';

const router = Router();

router.get('/', (req, res) => {
    res.send('index');
});

export const indexRouter = router;

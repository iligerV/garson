import { Router } from 'express';

const router = Router();

router.get('/', (req, res) => {
    res.send('BASKET_ALL');
});

export const basketRouter = router;

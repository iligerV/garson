import React, { FC } from 'react';
import { HashRouter } from 'react-router-dom';
// Components
import { TopPanel } from './TopPanel';
import { Main } from './Main';

export const App: FC = () => {
    return (
        <HashRouter>
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    minHeight: '100vh',
                }}
            >
                <TopPanel />
                <Main />
                <div style={{ height: '60px' }} />
            </div>
        </HashRouter>
    );
};

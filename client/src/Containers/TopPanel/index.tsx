import React from 'react';
// Components
import { Navigation } from './Navigation';
import MainLogo from 'client/src/Common/assets/main_logo.svg';

export const TopPanel: React.FC = () => {
    return (
        <div
            style={{
                position: 'sticky',
                left: 0,
                display: 'flex',
                justifyContent: 'space-between',
                height: '110px',
                maxWidth: 'calc(100vw - 64px)',
            }}
        >
            <div style={{ width: '300px' }} />
            <img
                src={MainLogo}
                alt={'MainLogo'}
                style={{ position: 'absolute', left: '50%', top: '38px' }}
            />
            <Navigation />
        </div>
    );
};

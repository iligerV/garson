import React from 'react';
import { NavLink } from 'react-router-dom';
// Config
import { ROUTING_SCHEMA } from 'client/src/Common/Configs';

export const Navigation: React.FC = () => {
    return (
        <div
            style={{
                width: '364px',
                display: 'flex',
                justifyContent: 'space-between',
                marginTop: '64px',
            }}
        >
            {ROUTING_SCHEMA.map(({ path, article }) => (
                <NavLink
                    key={path}
                    to={path}
                    style={{ color: 'rgba(44, 44, 44, 0.64)', textDecoration: 'none' }}
                    activeStyle={{ color: '#2C2C2C' }}
                >
                    {article}
                </NavLink>
            ))}
        </div>
    );
};

import React, { FC } from 'react';
// Assets
import NotFoundGif from 'client/src/Common/assets/not_found_404.gif';

const NotFound: FC = () => {
    return (
        <div style={{ margin: 'auto' }}>
            <img src={NotFoundGif} alt={'NotFoundGif'} />
            <div
                style={{
                    position: 'relative',
                    bottom: '130px',
                    textAlign: 'center',
                    fontSize: '40px',
                    color: '#234D60',
                }}
            >
                OOPS! PAGE NOT FOUND
            </div>
        </div>
    );
};

export default NotFound;

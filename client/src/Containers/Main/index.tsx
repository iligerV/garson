import React, { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
// Components
import NotFound from './Pages/NOT_FOUND';
// Models
import { APP_ROUT } from 'client/src/Models';
import { ROUTING_SCHEMA } from '../../Common/Configs/RoutingSchema';

export const Main: React.FC = () => {
    const loggedIn = true;

    const renderPage = (pageCode: APP_ROUT) => {
        const CurrentPage = lazy(() =>
            import(`./Pages/${pageCode.toUpperCase()}_PAGE`).catch(
                () => import(`client/src/Containers/Main/Pages/NOT_FOUND`)
            )
        );
        return <CurrentPage />;
    };

    return (
        <div style={{ display: 'flex', flexGrow: 1 }}>
            <Switch>
                <Route exact path="/">
                    {loggedIn ? <Redirect to={`/${APP_ROUT.MENU}`} /> : <div />}
                </Route>
                {ROUTING_SCHEMA.map(({ path }) => (
                    <Route key={path} exact path={`/${path}`}>
                        <Suspense fallback={<div>Загрузка</div>}>{renderPage(path)}</Suspense>
                    </Route>
                ))}
                <Route path="*" component={NotFound} />
            </Switch>
        </div>
    );
};

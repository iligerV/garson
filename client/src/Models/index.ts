export enum ROLE_NAME {
    ADMIN = 'admin',
    USER = 'user',
}

export enum APP_ROUT {
    MENU = 'menu',
    BASKET = 'basket',
    PROFILE = 'profile',
}

export enum APP_ROUT_ARTICLE {
    MENU = 'Меню',
    BASKET = 'Корзина',
    PROFILE = 'Личный кабинет',
}

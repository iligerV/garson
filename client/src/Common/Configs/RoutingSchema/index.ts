import { APP_ROUT, APP_ROUT_ARTICLE } from 'client/src/Models';

export const ROUTING_SCHEMA = [
    {
        path: APP_ROUT.MENU,
        article: APP_ROUT_ARTICLE.MENU,
    },
    {
        path: APP_ROUT.BASKET,
        article: APP_ROUT_ARTICLE.BASKET,
    },
    {
        path: APP_ROUT.PROFILE,
        article: APP_ROUT_ARTICLE.PROFILE,
    },
];

import 'core-js/stable';
import 'normalize.css';
import { render } from 'react-dom';
import React from 'react';
// Components
import { App } from './Containers/App';

render(<App />, document.getElementById('root-app'));

// Types
import { Configuration as WebpackConfig } from 'webpack';
import { Configuration as WebpackDevServerConfig } from 'webpack-dev-server';
// Utils
import * as path from 'path';
// Plugins
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { TsconfigPathsPlugin } from 'tsconfig-paths-webpack-plugin';
// Options
import { devServer, getOptimizationOptions, getPlugins, getStyleLoaders } from './webpackOptions';

interface Configuration extends WebpackConfig {
    devServer?: WebpackDevServerConfig;
}
type AppMode = 'development' | 'production';

const mode = process.env.NODE_ENV as AppMode;
const isDevelopment = mode === 'development';
const isProduction = !isDevelopment;

const config: Configuration = {
    mode,
    entry: {
        index: path.resolve(__dirname, './client/src/index.tsx'),
    },
    output: {
        filename: '[name].[hash].js',
        path: path.resolve(__dirname, './dist'),
        libraryTarget: 'umd',
    },
    devtool: isDevelopment ? 'inline-source-map' : false,
    optimization: getOptimizationOptions(isProduction),
    devServer: isDevelopment ? devServer : undefined,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    configFile: `tsconfig.json`,
                    transpileOnly: true,
                },
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: isProduction ? MiniCssExtractPlugin.loader : 'style-loader',
                    },
                    'css-loader',
                ],
            },
            {
                test: /\.s[ac]ss$/,
                use: getStyleLoaders(isProduction),
            },
            {
                test: /\.svg|eot|ttf|woff|woff2|ico|png|gif|jpg($|\?)/,
                loader: 'file-loader',
                options: {
                    name: '[name][hash].[ext]',
                    outputPath: 'assets',
                },
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.css', '.json'],
        plugins: [new TsconfigPathsPlugin()],
    },
    plugins: isProduction
        ? getPlugins([
              new MiniCssExtractPlugin({
                  filename: '[name].[contenthash].css',
                  chunkFilename: '[id].[contenthash].css',
              }),
          ])
        : getPlugins(),
};

module.exports = config;

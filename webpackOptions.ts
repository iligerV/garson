import { Options } from 'webpack';
import { Configuration as WebpackDevServerConfig } from 'webpack-dev-server';
// Utils
import * as os from 'os';
import * as path from 'path';
// Plugins
import TerserPlugin from 'terser-webpack-plugin';
import OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';

const cores = os.cpus().length;

export const getOptimizationOptions = (isProduction): Options.Optimization => ({
    splitChunks: {
        chunks: 'all',
    },
    minimize: isProduction,
    minimizer: [new TerserPlugin({ cache: true, parallel: cores }), new OptimizeCssAssetsPlugin()],
});

export const devServer: WebpackDevServerConfig = {
    compress: true,
    port: 8082,
    host: '0.0.0.0',
    useLocalIp: true,
    hot: true,
    inline: true,
    stats: 'errors-only',
    clientLogLevel: 'warning',
    overlay: true,
    proxy: {
        '/api': 'http://localhost:3001',
    },
};

export const getStyleLoaders = (isProduction) => [
    {
        loader: isProduction ? MiniCssExtractPlugin.loader : 'style-loader',
    },
    {
        loader: 'css-loader',
        options: {
            modules: {
                mode: 'local',
                exportGlobals: true,
                localIdentName: '[name].[local].[hash:base64:5]',
                localIdentHashPrefix: 'hash',
            },
            esModule: true,
        },
    },
    'sass-loader',
];

export const getPlugins = (plugins: any[] = []) => [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'client/src', 'index.html'),
    }),
    new ForkTsCheckerWebpackPlugin({
        eslint: {
            files: ['./client/src/**/*.{ts,tsx,js,jsx}', './api/**/*.{ts,tsx,js,jsx}'],
        },
    }),
    ...plugins,
];
